import os
from flask import Flask, Blueprint
from config import Config
from flask_mongoengine import MongoEngine

app = Flask(__name__, static_folder=os.path.join(os.path.dirname(__file__), "static"))
app.config.from_object(Config)

db = MongoEngine(app)

MODULE_ROOT = os.path.dirname(os.path.abspath(__file__))
MODULE_STATIC = os.path.join(MODULE_ROOT, 'static')
MODULE_STORAGE = os.path.join(MODULE_STATIC, 'storage')

main = Blueprint('main', __name__)

from application import views

app.register_blueprint(main)


