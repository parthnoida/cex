from application import main
from flask import current_app, redirect
import requests
from application.models import Ticker
from mongoengine.queryset.visitor import Q
from application.utils import write_csv, del_file, write_csv_json
import datetime
import json


@main.route('/')
def index():
    return current_app.make_response("Welcome to CEX Algorithmic Trading:)")


@main.route('/log_ticker/')
def log_ticker():
    resp = requests.get('https://cex.io/api/tickers/USD/ETH/BTC')
    if resp.status_code == 200:
        ticker_data = json.loads(resp.text)
        ticker = Ticker().populate(ticker_data)
        ticker.save()
        return current_app.make_response(str(ticker_data))
    else:
        return resp.status_code


@main.route('/get_csv/')
@main.route('/get_csv/<st_date>/')
@main.route('/get_csv/<st_date>/<ed_date>/')
def get_csv(st_date=None, ed_date='2099-01-01:00:00'):
    if st_date is not None:
        st_timestamp = int(datetime.datetime.strptime(st_date, '%Y-%m-%d:%H:%M').strftime('%s'))
        ed_timestamp = int(datetime.datetime.strptime(ed_date, '%Y-%m-%d:%H:%M').strftime('%s'))

        ticker = Ticker.objects(Q(timestamp__gte=st_timestamp) & Q(timestamp__lte=ed_timestamp))

        # It will return a QuerySet, i.e. Cursor handling is taken care of.
        print len(ticker)

        filename = datetime.datetime.utcnow().strftime('%s') + '.csv'
        write_csv(filename, ticker)
        return redirect('/static/storage/' + filename)
    else:
        return current_app.make_response("please enter start & end(optional) datetime in %Y-%m-%d:%H:%M format separated by '/'")


@main.route('/get_ohlcv/')
@main.route('/get_ohlcv/<st_date>/<pair1>/<pair2>/')
def get_ohlcv_csv(st_date=None, pair1='ETH', pair2='USD'):
    if st_date is not None:
        prefix = pair1.lower() + "_"
        st_dt = datetime.datetime.strptime(st_date, '%Y-%m-%d').strftime('%Y%m%d')

        resp = requests.get('https://cex.io/api/ohlcv/hd/%s/%s/%s' % (st_dt, pair1, pair2))
        if resp.status_code == 200:
            data = json.loads(resp.text)
            filename = st_dt + pair1 + pair2 + '.csv'
            write_csv_json(filename, json.loads(data["data1m"]), prefix, mode='ab')
            return redirect('/static/storage/' + filename)
        else:
            return current_app.make_response(str(resp.status_code))
    else:
        return current_app.make_response("please enter start datetime, in %Y-%m-%d format, currency code 1 and currency code 2 separated by '/'")


@main.route('/del_csv/<filename>/')
def del_csv(filename):
    return current_app.make_response(str(del_file(filename)))
