from application import MODULE_STORAGE
import csv
import os
import datetime


def del_file(filename):
    return os.remove(os.path.join(MODULE_STORAGE, filename))


def write_csv(filename, data, headers=[], mode='w'):
    # data  = List of objects
    # headers = list having header fields
    # mode = write new, append

    if len(headers) == 0:
        headers = ['timestamp', 'date', 'btc_last', 'btc_low', 'btc_high', 'btc_vol', 'btc_vol30', 'btc_bid', 'btc_ask',
                   'eth_last', 'eth_low', 'eth_high', 'eth_vol', 'eth_vol30', 'eth_bid', 'eth_ask', 'eth_btc_last',
                   'eth_btc_low', 'eth_btc_high', 'eth_btc_vol', 'eth_btc_vol30', 'eth_btc_bid', 'eth_btc_ask']

    if os.path.exists(os.path.join(MODULE_STORAGE, filename)):
        # If file already exists, simple append data
        # delete file and overwrite data
        os.remove(os.path.join(MODULE_STORAGE, filename))

    with open(os.path.join(MODULE_STORAGE, filename), mode) as f:
        writer = csv.writer(f)
        writer.writerow(headers)
        for d_rw in data:
            date = datetime.datetime.fromtimestamp(d_rw.timestamp).strftime("%Y-%m-%d %H:%M")
            rw = [d_rw.timestamp, date, d_rw.btc_last, d_rw.btc_low, d_rw.btc_high, d_rw.btc_vol, d_rw.btc_vol30, d_rw.btc_bid, d_rw.btc_ask]
            rw.extend([d_rw.eth_last, d_rw.eth_low, d_rw.eth_high, d_rw.eth_vol, d_rw.eth_vol30, d_rw.eth_bid, d_rw.eth_ask])
            rw.extend([d_rw.eth_btc_last, d_rw.eth_btc_low, d_rw.eth_btc_high, d_rw.eth_btc_vol, d_rw.eth_btc_vol30, d_rw.eth_btc_bid, d_rw.eth_btc_ask])
            writer.writerow(rw)

    return


def write_csv_json(filename, data, prefix, headers=[], mode='w'):
    # data  = List of objects
    # headers = list having header fields
    # mode = write new, append

    if len(headers) == 0:
        headers = ['timestamp', 'date', prefix+'open', prefix+'high', prefix+'low', prefix+'close', prefix+'vol']

    if os.path.exists(os.path.join(MODULE_STORAGE, filename)):
        # If file already exists, simple append data
        # delete file and overwrite data
        os.remove(os.path.join(MODULE_STORAGE, filename))

    # if file doesn't exist, first add headers
    with open(os.path.join(MODULE_STORAGE, filename), mode) as f:
        writer = csv.writer(f)
        writer.writerow(headers)
        for d_rw in data:
            date = datetime.datetime.fromtimestamp(d_rw[0]).strftime("%Y-%m-%d %H:%M")
            rw = [d_rw[0], date, d_rw[1], d_rw[2], d_rw[3], d_rw[4], d_rw[5]]
            writer.writerow(rw)

    return
