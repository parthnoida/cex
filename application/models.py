from application import db


class Ticker(db.Document):
    timestamp = db.LongField(primary_key=True)

    btc_last = db.FloatField(db_field='b')
    btc_low = db.FloatField(db_field='bl')
    btc_high = db.FloatField(db_field='bh')
    btc_vol = db.FloatField(db_field='bv')
    btc_vol30 = db.FloatField(db_field='bv30')
    btc_bid = db.FloatField(db_field='bb')
    btc_ask = db.FloatField(db_field='ba')

    eth_last = db.FloatField(db_field='e')
    eth_low = db.FloatField(db_field='el')
    eth_high = db.FloatField(db_field='eh')
    eth_vol = db.FloatField(db_field='ev')
    eth_vol30 = db.FloatField(db_field='ev30')
    eth_bid = db.FloatField(db_field='eb')
    eth_ask = db.FloatField(db_field='ea')

    eth_btc_last = db.FloatField(db_field='ebt')
    eth_btc_low = db.FloatField(db_field='ebtl')
    eth_btc_high = db.FloatField(db_field='ebth')
    eth_btc_vol = db.FloatField(db_field='ebtv')
    eth_btc_vol30 = db.FloatField(db_field='ebtv30')
    eth_btc_bid = db.FloatField(db_field='ebtb')
    eth_btc_ask = db.FloatField(db_field='ebta')

    def populate(self, ticker_json):
        data = ticker_json['data']  # List of dicts
        for pair in data:
            if pair['pair'] == 'BTC:USD':
                self.timestamp = int(pair['timestamp'])
                self.btc_last = float(pair['last'])
                self.btc_low = float(pair['low'])
                self.btc_high = float(pair['high'])
                self.btc_vol = float(pair['volume'])
                self.btc_vol30 = float(pair['volume30d'])
                self.btc_bid = float(pair['bid'])
                self.btc_ask = float(pair['ask'])
            elif pair['pair'] == 'ETH:USD':
                self.eth_last = float(pair['last'])
                self.eth_low = float(pair['low'])
                self.eth_high = float(pair['high'])
                self.eth_vol = float(pair['volume'])
                self.eth_vol30 = float(pair['volume30d'])
                self.eth_bid = float(pair['bid'])
                self.eth_ask = float(pair['ask'])
            elif pair['pair'] == 'ETH:BTC':
                self.eth_btc_last = float(pair['last'])
                self.eth_btc_low = float(pair['low'])
                self.eth_btc_high = float(pair['high'])
                self.eth_btc_vol = float(pair['volume'])
                self.eth_btc_vol30 = float(pair['volume30d'])
                self.eth_btc_bid = float(pair['bid'])
                self.eth_btc_ask = float(pair['ask'])

        return self

    def __repr__(self):
        super(Ticker, self).__repr__()
        return str(self.timestamp)


