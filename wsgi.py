import os
from werkzeug.contrib.fixers import ProxyFix
from application import app

app.wsgi_app = ProxyFix(app.wsgi_app)
app.debug = bool(os.environ.get('PRODUCTION'))

if __name__ == '__main__':
    app.run()